// File        : Pool.Java
// Description : Class that allows pooling of objects

/*Class which enables objects to be pooled easily. Example of usage:
* 
* PoolObjectFactory<TouchEvent>factory=new PoolObjectFactory<TouchEvent>() {
* 	@Override
* 	public TouchEvent createObject() {
* 		return new TouchEvent();
* 	}
* };
* Pool<TouchEvent>touchEventPool=new Pool<TouchEvent>(factory, 50);
* TouchEvent touchEvent=touchEventPool.newObject();
* ... do something here ...
* touchEventPool.free(touchEvent);
*/

package com.asterdroids.framework;

import java.util.ArrayList;
import java.util.List;

// Templated pool class
public class Pool<T> {

	// Templated factory class
	public interface PoolObjectFactory<T> {
		public T createObject();
	}
	
	
	private final List<T> freeObjects;           // List of available objects
	private final PoolObjectFactory<T> factory;  // Factory object
	private final int maxSize;                   // Maximum pool size
	
	// Constructor
	public Pool(PoolObjectFactory<T> _factory, int _maxSize) {
		
		// Set factory and maximum pool size
		this.factory = _factory; 
		this.maxSize = _maxSize;
		
		// Allocate memory for pool
		this.freeObjects = new ArrayList<T>(maxSize);
	}
	
	// Get an object from the pool
	public T newObject() {
		
		T object = null;
		
		// If pool is empty, use factory to create object
		// Otherwise take available object from end of pool
		if(freeObjects.isEmpty()) {
			object = factory.createObject();
		}
		else {
			object = freeObjects.remove(freeObjects.size()-1);
		}
		
		return object;
	}
	
	// Free up object currently in use
	public void free(T object) {
		if(freeObjects.size() < maxSize) {
			freeObjects.add(object);
		}
	}
	
}
