// File        : FileIO.Java
// Description : Interface for asterdroids file input/output facilities 


package com.asterdroids.framework;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface FileIO {

	// Read asset into InputStream
	public InputStream readAsset(String fileName) throws IOException;
	
	// Read file into InputStream
	public InputStream readFile(String fileName) throws IOException;
	
	// Write file to OutputStream
	public OutputStream writeFile(String fileName) throws IOException;
}
