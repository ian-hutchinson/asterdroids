// File        : Game.Java
// Description : Interface for asterdroids game framework

package com.asterdroids.framework;

public interface Game {

	// Get input object
	public Input getInput();
	
	// Get file IO object
	public FileIO getFileIO();
	
	// Get audio object
	public Audio getAudio();
	
	// Get graphics object
	public Graphics getGraphics();
	
	// Set screen to be displayed
	public void setScreen(Screen screen);
	
	// Get currently displayed screen
	public Screen getCurrentScreen();
	
	// Get start menu screen
	public Screen getStartScreen();

	// Close game
	public void close();
}
