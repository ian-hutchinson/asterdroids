// File        : Graphics.Java
// Description : Interface for asterdroids graphics framework
//               Handles display of assets to screen

package com.asterdroids.framework;
import java.lang.String;

import android.content.Context;

public interface Graphics {
	
	// Pixmap ARGB format enum
	public static enum PixmapFormat {
		ARGB8888, ARGB4444, RGB565;
	}
	
	// Create new Pixmap
	public Pixmap newPixmap(String fileName, PixmapFormat format);
	
	// Clear screen
	public void clear(int color);
	
	// Draw a pixel at a coordinate
	public void drawPixel(int x, int y, int color);
	
	// Draw a line between two coordinates
	public void drawLine(int x, int y, int x2, int y2, int color);
	
	//draw text to screen. coordinates top left
	public void drawTextString(int x, int y, int size, int color, String text,Context context);
	
	//Draw a filled rectangle. Coordinates are top left.
	public void drawRect(int x, int y, int width, int height, int color);
	
	// Draw a filled circle
	public void drawCircle( float x, float y, float radius, int color );
	
	//Draw a portion of a pixmap. Coordinates are top left.
	public void drawPixmap(Pixmap pixmap, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight);
	
	//Draw whole pixmap. Coordinates are top left.
	public void drawPixmap(Pixmap pixmap, int x, int y);
	
	// Draw a pixmap with rotation
	public void drawPixmap(Pixmap pixmap, int x, int y, float rot);
	
	//Returns the width of the framebuffer
	public int getWidth();
	
	//Returns the height of the framebuffer
	public int getHeight();
}
