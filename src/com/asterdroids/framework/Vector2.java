// File        : Vector2.Java
// Description : Vector2 class used for vector calculations


/*2D Vector class
 * based on Vector2 found in Beginning Android Games 2nd Edition
 * Alexander Moss
 * 
 * Each function returns a reference to the working vector
 * so that operations can be linked e.g. v.add(10,10).mul(10).sub(14)
 * Required due to lack of operator overloading.
 */

package com.asterdroids.framework;

import java.lang.Math;
 
public class Vector2 {

	// Radian/Degree conversions facilities
	public static float TO_RADIANS = (1 / 180.0f) * (float) Math.PI;
	public static float TO_DEGREES = (1 / (float) Math.PI) * 180;
	
	// Member coordinates, x and y
	public float x, y;
	
	// Blank constructor, defaults to 0,0
	public Vector2() {
		this.x = 0;
		this.y = 0;
	}
	
	// Constructor taking in initial x,y values
	public Vector2(float _x, float _y) {
		this.x = _x;
		this.y = _y;
	}
	
	// Copy constructor
	public Vector2(Vector2 _other) {
		this.x = _other.x;
		this.y = _other.y;
	}
	
	// Copy, returns a copy of this vector
	// Remember, Java is reference based. Can't just return
	// "by value" objects.
	public Vector2 cpy() {
		return new Vector2(x, y);
	}
	
	// Set function, sets x and y coordinate to arguments passed in
	public Vector2 set(float _x, float _y) {
		this.x = _x;
		this.y = _y;
		return this;
	}
	
	// Set function, sets vector to vector passed in as argument
	public Vector2 set(Vector2 _other) {
		this.x = _other.x;
		this.y = _other.y;
		return this;
	}
	
	// Add x and y coordinates to vector
	public Vector2 add(float _x, float _y) {
		this.x += _x;
		this.y += _y;
		return this;
	}
	
	// Add another vector to this vector
	public Vector2 add(Vector2 _other) {
		this.x += _other.x;
		this.y += _other.y;
		return this;
	}
	
	// Subtract from x and y coordinates of vector
	public Vector2 sub(float _x, float _y) {
		this.x -= _x;
		this.y -= _y;
		return this;
	}
	
	// Subtract another vector from this vector
	public Vector2 sub(Vector2 _other) {
		this.x -= _other.x;
		this.y -= _other.y;
		return this;
	}
	
	// Multiply vector by scalar value
	public Vector2 mul(float _scalar) {
		this.x *= _scalar;
		this.y *= _scalar;
		return this;
	}
	
	// Get length (magnitude) of vector
	public float len() {
		return (float) Math.sqrt(x*x + y*y);
	}
	
	// Normalise vector (produce a unit vector)
	// Makes the magnitude == 1
	public Vector2 normal() {
		float len = this.len();
		if(len != 0) {
			this.x /= len;
			this.y /= len;
		}
		return this;
	}
	
	// Returns bearing of vector in degrees
	public float angle() {
		float angle = (float) Math.atan2(y, x) * TO_DEGREES;
		if(angle < 0) {
			angle += 360;
		}
		return angle;
	}
	
	// Rotate vector using standard vector rotation methods
	// Argument should be in degrees
	public Vector2 rotate(float _angle) {
		float rad = _angle * TO_RADIANS;
		float cos = (float) Math.cos(rad);
		float sin = (float) Math.sin(rad);
		
		float newX = this.x * cos - this.y * sin;
		float newY = this.x * sin + this.y * cos;
		
		this.x = newX;
		this.y = newY;
		
		return this;
	}
	
	// Returns the distance between this vector and vector in argument
	// Essentially the magnitude between the two vectors
	public float distance(Vector2 _other) {
		float distX = this.x - _other.x;
		float distY = this.y - _other.y;
		
		//Pythagoras
		return (float) Math.sqrt(distX * distX + distY * distY);
	}
	
	// Returns the distance (magnitude) between this vector and a fixed
	// coordinate
	public float distance(float _x, float _y) {
		float distX = this.x - _x;
		float distY = this.y - _y;
		
		//Pythagoras
		return (float) Math.sqrt(distX * distX + distY * distY);
	}
	
	// Function to negate vector (inverses elements)
	public Vector2 negate() {
		this.x = -this.x;
		this.y = -this.y;
		
		return this;
	}
}
