// File        : Pixmap.Java
// Description : Interface for asterdroids Pixmap object.

package com.asterdroids.framework;

import com.asterdroids.framework.Graphics.PixmapFormat;

public interface Pixmap {
	
	// Accessor functions for width and height
	public int getWidth();
	
	public int getHeight();
	
	// Accessor function returning Pixmap ARGB format
	public PixmapFormat getFormat();
	
	// Disposal function
	public void dispose();
}
