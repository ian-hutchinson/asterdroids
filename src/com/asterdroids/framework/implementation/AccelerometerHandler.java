// File        : AccelerometerHandler.Java
// Description : Implementation of class to handle readings from
//               the device's accelerometer


/* Class implements accelerometer handler found in Beginning Android Games 2nd Edition
 * 
 */

package com.asterdroids.framework.implementation;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class AccelerometerHandler implements SensorEventListener {

	// X, Y and Z member variables
	float accelX;
	float accelY;
	float accelZ;
	
	// Constructor
	public AccelerometerHandler(Context context) {
		
		SensorManager manager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		
		// If an accelerometer exists, add this class as a listener
		if(manager.getSensorList(Sensor.TYPE_ACCELEROMETER).size() != 0) {
			
			Sensor accelerometer = manager.getSensorList(Sensor.TYPE_ACCELEROMETER).get(0);
			
			manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
		}
	}
	
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		
		//nothing

	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		// Get X, Y and Z values when listener detects a change
		accelX = event.values[0]; 
		accelY = event.values[1];
		accelZ = event.values[2];
	}
	
	// Accessor functions for X, Y and Z values
	public float getAccelX() {
		return accelX;
	}
	
	public float getAccelY() {
		return accelY;
	}
	
	public float getAccelZ() {
		return accelZ;
	}
}
