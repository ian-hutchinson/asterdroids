// File        : AndroidFileIO.Java
// Description : Implementation of FileIO class


package com.asterdroids.framework.implementation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.asterdroids.framework.FileIO;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Environment;
import android.preference.PreferenceManager;

public class AndroidFileIO implements FileIO {
	Context context;
	AssetManager assets;         // Assets
	String externalStoragePath;  // Path where external files are stored
	
	public AndroidFileIO(Context context) {
		this.context = context;
		this.assets = context.getAssets();
		this.externalStoragePath = Environment.getExternalStorageDirectory()
				.getAbsolutePath()+File.separator;
	}
	
	// Read asset given filename
	public InputStream readAsset(String fileName) throws IOException {
		return assets.open(fileName);
	}
	
	// Read file given filename
	// Used to read control preferences on launch
	public InputStream readFile(String fileName) throws IOException {
		return new FileInputStream(externalStoragePath+fileName);
	}
	
	// Write to file.
	// Used to store control preferences
	public OutputStream writeFile(String fileName) throws IOException {
		return new FileOutputStream(externalStoragePath+fileName);
	}
	
	// Accessor function for preferences
	public SharedPreferences getPreferences() {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
}
