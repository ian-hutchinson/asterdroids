// File        : AndroidInput.Java
// Description : Implementation of input class 

package com.asterdroids.framework.implementation;

import java.util.List;

import android.content.Context;
import android.view.View;

import com.asterdroids.framework.Input;

public class AndroidInput implements Input {

	// Three handlers - accelerometer, touch and keyboard
	AccelerometerHandler accelHandler;
	TouchHandler touchHandler;
	KeyboardHandler keyboardHandler;
	
	public AndroidInput(Context context, View view, float scaleX, float scaleY) {
		// Create new handler objects
		accelHandler = new AccelerometerHandler(context);
		touchHandler = new MultiTouchHandler(view, scaleX, scaleY);
		keyboardHandler = new KeyboardHandler(view);
	}
	
	@Override
	// Return when screen is being touched
	public boolean isTouchDown(int pointer) {
		return touchHandler.isTouchDown(pointer);
	}

	@Override
	// Return touch x coordinate
	public int getTouchX(int pointer) {
		return touchHandler.getTouchX(pointer);
	}

	@Override
	// Return touch y coordinate
	public int getTouchY(int pointer) {
		return touchHandler.getTouchY(pointer);
	}

	// Accessor functions for X,Y and Z value of accelerometer
	@Override
	public float getAccelX() {
		return accelHandler.getAccelX();
	}

	@Override
	public float getAccelY() {
		return accelHandler.getAccelY();
	}

	@Override
	public float getAccelZ() {
		return accelHandler.getAccelZ();
	}

	// Return list of touch events
	@Override
	public List<TouchEvent> getTouchEvents() {
		return touchHandler.getTouchEvents();
	}

	// Check to see if a key is being pressed
	@Override
	public boolean isKeyPressed(int keyCode) {
		return keyboardHandler.isKeyPressed(keyCode);
	}

	// Return a list of key events
	@Override
	public List<KeyEvent> getKeyEvents() {
		return keyboardHandler.getKeyEvents();
	}

}
