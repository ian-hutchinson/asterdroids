// File        : AndroidSound.Java
// Description : Implementation of sound class

package com.asterdroids.framework.implementation;

import android.media.SoundPool;

import com.asterdroids.framework.Sound;

public class AndroidSound implements Sound {
	int soundId;         // Identifier
	SoundPool soundPool; // Sound pool
	
	// Constructor
	public AndroidSound(SoundPool soundPool, int soundId) {
		this.soundId = soundId;
		this.soundPool = soundPool;
	}

	// Play sound at volume
	@Override
	public void play(float volume) {
		soundPool.play(soundId, volume, volume, 0, 0, 1);
	}

	// Dispose, releases resources
	@Override
	public void dispose() {
		soundPool.unload(soundId);
	}

	@Override
	public void stop() {
		soundPool.stop(soundId);
		
	}
}
