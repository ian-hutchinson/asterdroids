// File        : MultiTouchHandler.Java
// Description : Implementation of multi touch handler

package com.asterdroids.framework.implementation;

import java.util.ArrayList;
import java.util.List;

import android.view.MotionEvent;
import android.view.View;
import com.asterdroids.framework.Pool;
import com.asterdroids.framework.Input.TouchEvent;
import com.asterdroids.framework.Pool.PoolObjectFactory;

public class MultiTouchHandler implements TouchHandler {
	private static final int MAX_TOUCHPOINTS = 10;      // Maximum number of on-screen touches
	
	boolean[] isTouched = new boolean[MAX_TOUCHPOINTS]; // Create flag for each touch point
	
	int[] touchX = new int[MAX_TOUCHPOINTS];  // Create x coord for each touch point
	int[] touchY = new int[MAX_TOUCHPOINTS];  // Create y coord for each touch point
	int[] id = new int[MAX_TOUCHPOINTS];      // Create id for each touch point
	
	Pool<TouchEvent> touchEventPool;                                    // Touch event pool
	List<TouchEvent> touchEvents = new ArrayList<TouchEvent>();         // List of touch events
	List<TouchEvent> touchEventsBuffer = new ArrayList<TouchEvent>();   // Touch event buffer
	
	// Scale values
	float scaleX;
	float scaleY;
	
	// Constructor
	public MultiTouchHandler(View view, float _scaleX, float _scaleY) {
		
		// Create new factory
		PoolObjectFactory<TouchEvent> factory = new PoolObjectFactory<TouchEvent>() {
			public TouchEvent createObject() {
				return new TouchEvent();
			}
		};
		
		// Create touch event pool
		touchEventPool = new Pool<TouchEvent>(factory,100);
		
		// Set listener
		view.setOnTouchListener(this);
		
		// Assign scale values
		this.scaleX = _scaleX;
		this.scaleY = _scaleY;
	}

	// Listen for touches
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		synchronized(this) {
			int action = event.getAction() & MotionEvent.ACTION_MASK;
			
			int pointerIndex = (event.getAction() & MotionEvent.ACTION_POINTER_ID_MASK)
					>> MotionEvent.ACTION_POINTER_ID_SHIFT;
					
			int pointerCount = event.getPointerCount();
			
			// Touch event
			TouchEvent touchEvent;
			
			// Get a free touch
			for(int i=0; i<MAX_TOUCHPOINTS; i++) {
				if(i >= pointerCount) {
					isTouched[i] = false;
					id[i] = -1;
					continue;
				}
				
				int pointerId = event.getPointerId(i);
				
				if(event.getAction() != MotionEvent.ACTION_MOVE && i != pointerIndex) {
					continue;
				}
				
				// Various cases
				// First handles finger presses, second finger releases,
				// third drags.
				// In any event, the isTouch flag and coordinates are set accorindingly
				switch(action) {
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_POINTER_DOWN:
					touchEvent = touchEventPool.newObject();
					touchEvent.type = TouchEvent.TOUCH_DOWN;
					touchEvent.pointer = pointerId;
					touchEvent.x = touchX[i] = (int)(event.getX(i)* scaleX);
					touchEvent.y = touchY[i] = (int)(event.getY(i)* scaleY);
					isTouched[i] = true;
					id[i] = pointerId;
					touchEventsBuffer.add(touchEvent);
					break;
					
				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_POINTER_UP:
				case MotionEvent.ACTION_CANCEL:
					touchEvent = touchEventPool.newObject();
					touchEvent.type = TouchEvent.TOUCH_UP;
					touchEvent.pointer = pointerId;
					touchEvent.x = touchX[i] = (int)(event.getX(i)* scaleX);
					touchEvent.y = touchY[i] = (int)(event.getY(i)* scaleY);
					isTouched[i] = false;
					id[i] = -1;
					touchEventsBuffer.add(touchEvent);
					break;
					
				case MotionEvent.ACTION_MOVE:
					touchEvent = touchEventPool.newObject();
					touchEvent.type = TouchEvent.TOUCH_DRAGGED; 
					touchEvent.pointer = pointerId; 
					touchEvent.x=touchX[i]=(int) (event.getX(i) * scaleX); 
					touchEvent.y=touchY[i]=(int) (event.getY(i) * scaleY); 
					isTouched[i] = true;
					id[i] = pointerId;
					touchEventsBuffer.add(touchEvent);
					break;
				}
			}
			return true;
		}
	}

	// Returns whether there is currently a touch 
	@Override
	public boolean isTouchDown(int pointer) {
		synchronized(this) {
			int index = getIndex(pointer);
			if(index < 0 || index >= MAX_TOUCHPOINTS)
				return false;
			else
				return isTouched[index];
		}
	}

	// Get touch x coordinate
	@Override
	public int getTouchX(int pointer) {
		synchronized(this) {
			int index = getIndex(pointer);
			if(index < 0 || index >= MAX_TOUCHPOINTS)
				return 0;
			else
				return touchX[index];
		}
	}

	// Get touch y coordinate
	@Override
	public int getTouchY(int pointer) {
		synchronized(this) {
			int index = getIndex(pointer);
			if(index < 0 || index >= MAX_TOUCHPOINTS)
				return 0;
			else
				return touchY[index];
		}
	}

	// Get a list of touch events
	@Override
	public List<TouchEvent> getTouchEvents() {
		synchronized(this) {
			int len = touchEvents.size();
			for(int i=0; i<len; i++) {
				touchEventPool.free(touchEvents.get(i));
			}
			touchEvents.clear();
			touchEvents.addAll(touchEventsBuffer);
			touchEventsBuffer.clear();
			return touchEvents;
		}
	}
	
	//Method to access the index for a given pointerId in array
	private int getIndex(int pointerId) {
		for(int i=0;i<MAX_TOUCHPOINTS;i++) {
			if(id[i] == pointerId) {
				return i;
			}
		}
		return -1;
	}
}
