// File        : TouchHandler.Java
// Description : Interface for asterdroids touch handler

package com.asterdroids.framework.implementation;

import java.util.List;

import android.view.View.OnTouchListener;

import com.asterdroids.framework.Input.TouchEvent;


public interface TouchHandler extends OnTouchListener {

	// Return whether touch is present
	public boolean isTouchDown(int pointer);
	
	// Get touch x coordinate
	public int getTouchX(int pointer);
	
	// Get touch y coordinate
	public int getTouchY(int pointer);
	
	// Get list of touch events
	public List<TouchEvent> getTouchEvents();
}
