// File        : AndroidFastRenderView.Java
// Description : Implementation of threaded rendering class
//               Essentially handles the drawing functionality

package com.asterdroids.framework.implementation;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
public class AndroidFastRenderView extends SurfaceView implements Runnable {

	AndroidGame game;                  // Reference of game object
	Bitmap frameBuffer;                // Framebuffer, used to draw assets before scaling
	Thread renderThread = null;        // Thread used for render jobs
	SurfaceHolder holder;              // Holder used to get surfaces
	volatile boolean running = false;  // Boolean to represent whether game is running
	
	public AndroidFastRenderView(AndroidGame _game, Bitmap _frameBuffer) {
		super(_game);
		
		// Set game, framebuffer and holder
		this.game = _game;  
		this.frameBuffer = _frameBuffer;
		this.holder = getHolder();
	}
	
	public void resume() {
		
		// When the game is resumed, set the running flag
		// Create a new thread for rendering
		running = true;
		renderThread = new Thread(this);
		renderThread.start();
	}

	@Override
	public void run() {
		Rect dstRect = new Rect(); // Rect will be used to get screen dimensions
		
		long startTime = System.nanoTime();
		
		while(running) {
			if(!holder.getSurface().isValid())
				continue;
			
			//Calculate the period of the previous frame and store in deltaTime
			float deltaTime = (System.nanoTime()-startTime) / 1000000000.0f;
			startTime = System.nanoTime();
			
			// Update and Draw screen based on time
			game.getCurrentScreen().update(deltaTime);
			game.getCurrentScreen().present(deltaTime);
			
			Canvas canvas = holder.lockCanvas();                 // Lock canvas
			canvas.getClipBounds(dstRect);                       // sets dstRect to dimensions of screen
			canvas.drawBitmap(frameBuffer, null, dstRect, null); // Draw to canvas
			holder.unlockCanvasAndPost(canvas);                  // Unlock canvas
		}
	}
	
	public void pause() {
		// When the game is paused, disable the running flag
		running = false;
		
		// Join the current rendering thread back to the main thread
		while(true) {
			try {
				renderThread.join();
				return;
			} catch (InterruptedException e) {
				
			}
		}
	}

}
