// File        : KeyboardHandler.Java
// Description : Implementation of class that handles keyboard touch events

package com.asterdroids.framework.implementation;

import java.util.ArrayList;
import java.util.List;

import com.asterdroids.framework.Input.KeyEvent;
import com.asterdroids.framework.Pool;
import com.asterdroids.framework.Pool.PoolObjectFactory;

import android.view.View;
import android.view.View.OnKeyListener;

public class KeyboardHandler implements OnKeyListener {
	boolean[] pressedKeys = new boolean[128];                    // Key array
	Pool<KeyEvent> keyEventPool;                                 // Event pool
	List<KeyEvent> keyEventsBuffer = new ArrayList<KeyEvent>();  // Events buffer
	List<KeyEvent> keyEvents = new ArrayList<KeyEvent>();        // Event list
	
	// Constuctor
	public KeyboardHandler(View view) {
		
		// Create pool factor
		PoolObjectFactory<KeyEvent> factory = new PoolObjectFactory<KeyEvent>() {
			public KeyEvent createObject() {
				return new KeyEvent();
			}
		};
		
		// Create pool
		keyEventPool = new Pool<KeyEvent>(factory,100);
		
		view.setOnKeyListener(this);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
	}

	// Checks for key press and sets flag in key array to true
	@Override
	public boolean onKey(View v, int keyCode, android.view.KeyEvent event) {
		if(event.getAction() == android.view.KeyEvent.ACTION_MULTIPLE)
			return false;
		
		synchronized(this) {
			KeyEvent keyEvent = keyEventPool.newObject();
			keyEvent.keyCode = keyCode;
			keyEvent.keyChar = (char)event.getUnicodeChar();
			
			// If key is pressed and value is within the key array range,
			// set corresponding flag to true
			if(event.getAction() == android.view.KeyEvent.ACTION_DOWN){ 
				keyEvent.type = KeyEvent.KEY_DOWN;
				if(keyCode>0 && keyCode< 127)
					pressedKeys[keyCode] = true;
			}
			
			// If key isn't pressed and value is within key array range,
			// set corresponding flag to false
			if(event.getAction() == android.view.KeyEvent.ACTION_UP){
				keyEvent.type = KeyEvent.KEY_UP;
				if(keyCode > 0 && keyCode < 127)
					pressedKeys[keyCode] = false;
			}
			keyEventsBuffer.add(keyEvent);
		}
		return false;
	}
	
	// Returns whether key in array is pressed
	public boolean isKeyPressed(int keyCode) {
		if(keyCode < 0 || keyCode > 127)
			return false;
		return pressedKeys[keyCode];
	}
	
	// Returns a list of key events
	public List<KeyEvent>getKeyEvents() {
		synchronized(this) {
			int len = keyEvents.size();
			for(int i=0; i<len; i++) {
				keyEventPool.free(keyEvents.get(i));
			}
			keyEvents.clear();
			keyEvents.addAll(keyEventsBuffer);
			keyEventsBuffer.clear();
			return keyEvents;
		}
	}

}
