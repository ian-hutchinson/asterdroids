// File        : AndroidAudio.Java
// Description : Implementation for the android audio class

package com.asterdroids.framework.implementation;

import java.io.IOException;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;

import com.asterdroids.framework.Audio;
import com.asterdroids.framework.Music;
import com.asterdroids.framework.Sound;

public class AndroidAudio implements Audio {

	AssetManager assets; // Reference of asset manager to locate sound assets
	SoundPool soundPool; // SoundPool object
	
	//Constructor receives Activity in order to get handle on Assets
	public AndroidAudio(Activity activity) {
		activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
		this.assets = activity.getAssets();
		this.soundPool = new SoundPool(20,AudioManager.STREAM_MUSIC,0);
	}
	
	@Override
	public Music newMusic(String filename) {
		try {
			// Creates new music object
			// Throw an exception if file can't be loaded
			AssetFileDescriptor assetDescriptor = assets.openFd(filename);
			return new AndroidMusic(assetDescriptor);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load music " + filename);
		}
	}

	@Override
	public Sound newSound(String filename) {
		AssetFileDescriptor assetDescriptor;
		System.out.println("Oh NO!");
		try {
			// Creates a new sound object
			// Throws an exception if file can't be loaded
			assetDescriptor = assets.openFd(filename);
			int soundId = soundPool.load(assetDescriptor, 0);
			return new AndroidSound(soundPool, soundId);
		} catch (IOException e) {
			throw new RuntimeException("Couldn't load sound " + filename);
		}
		
	}

}
