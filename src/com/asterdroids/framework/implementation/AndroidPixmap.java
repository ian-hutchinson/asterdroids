// File        : AndroidPixmap.Java
// Description : Implementation of Android Pixmap class

package com.asterdroids.framework.implementation;

import android.graphics.Bitmap;

import com.asterdroids.framework.Pixmap;
import com.asterdroids.framework.Graphics.PixmapFormat;

public class AndroidPixmap implements Pixmap {

	Bitmap bitmap;        // Bitmap to store image data
	PixmapFormat format;  // Pixmap formap
	
	// Constructor
	public AndroidPixmap(Bitmap _bitmap, PixmapFormat _format) {
		this.bitmap = _bitmap;
		this.format = _format;
	}
	
	
	// Accessor functions for width, height and format
	@Override
	public int getWidth() {
		return bitmap.getWidth();
	}

	@Override
	public int getHeight() {
		return bitmap.getHeight();
	}

	@Override
	public PixmapFormat getFormat() {
		return format;
	}

	// Disposal function, release resources
	@Override
	public void dispose() {
		bitmap.recycle();
	}

}
