// File        : AndroidGame.Java
// Description : Implementation of game class

package com.asterdroids.framework.implementation;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.asterdroids.framework.Audio;
import com.asterdroids.framework.FileIO;
import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Input;
import com.asterdroids.framework.Screen;

@SuppressLint("Wakelock")
public class AndroidGame extends Activity implements Game {

	AndroidFastRenderView renderView;  // Render view, for drawing
	Graphics graphics;                 // Graphics object
	Input input;                       // Input object
	Audio audio;                       // Audio object
	FileIO fileIO;                     // FileIO object, for settings
	Screen screen;                     // The screen
	WakeLock wakeLock;                 // Wakelock
	
	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// Create window
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		// Get orientation of screen
		boolean isLandscape = getResources().getConfiguration().orientation == 
				Configuration.ORIENTATION_LANDSCAPE;
		
		// Set frame buffer accordingly, based on whether screen is portrait or landscape
		// This should always be landscape
		int frameBufferWidth = isLandscape ? 1196 : 720;
		int frameBufferHeight = isLandscape ? 720 : 1196;
		
		// Create the framebuffer bitmap
		Bitmap frameBuffer = Bitmap.createBitmap(frameBufferWidth, frameBufferHeight, Config.RGB_565);
	
		// Calculate scales based on frame buffer and display size
		float scaleX = (float) frameBufferWidth /
				getWindowManager().getDefaultDisplay().getWidth(); //This is deprecated after API 13.
		float scaleY = (float) frameBufferHeight /
				getWindowManager().getDefaultDisplay().getHeight(); //This is deprecated after API 13.
		
		// Create game elements
		renderView = new AndroidFastRenderView(this, frameBuffer);
		graphics = new AndroidGraphics(getAssets(), frameBuffer);
		fileIO = new AndroidFileIO(this);
		audio = new AndroidAudio(this);
		input = new AndroidInput(this, renderView, scaleX, scaleY);
		
		// Set screen to start screen
		screen = getStartScreen();
		
		// Set content view to render view object, which will handle the
		// drawing on a separate thread
		setContentView(renderView);
		
		// Get wake lock
		PowerManager powerManager = (PowerManager)getSystemService(Context.POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, "Game");
	}
	
	@Override
	public void onResume() {
		super.onResume();
		Log.v("AndroidGame","onResume");
		wakeLock.acquire();  // Acquire wake lock. Don't want the screen to sleep.
		Log.v("AndroidGame","WakeLock acquired");
		screen.resume();     // Resume screen thread
		Log.v("AndroidGame","Screen resumed");
		renderView.resume(); // Resume render thread
		Log.v("AndroidGame","RenderView resumed");
	}
	
	@Override
	public void onPause() {
		super.onPause();
	
		wakeLock.release();  // Release wake lock
		renderView.pause();  // Pause render view. This will join drawing thread to main thread.
		screen.pause();      // Pause main screen
		
		if(isFinishing())
			screen.dispose(); // If app is closing, call screen's disposal function
	}
	
	// Accessor functions for input, FileIO and graphics
	@Override
	public Input getInput() {
		return input;
	}

	@Override
	public FileIO getFileIO() {
		return fileIO;
	}

	@Override
	public Graphics getGraphics() {
		return graphics;
	}

	// Set screen dictates which screen to show
	@Override
	public void setScreen(Screen screen) {
		// Handle null pointers
		if(screen == null)
			throw new IllegalArgumentException("Screen must not be null");
		
		// Destroy this screen and clean up
		this.screen.pause();
		this.screen.dispose();
		screen.resume();
		screen.update(0);
		
		// Set current screen to new screen
		this.screen = screen;
	}

	// Accessor functions to get current scre
	@Override
	public Screen getCurrentScreen() {
		return screen;
	}

	@Override
	public Screen getStartScreen() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void close() {
		this.finish();
	}

	// Accessor function for audio
	@Override
	public Audio getAudio() {
		return audio;
	}

}
