// File        : AndroidGraphics.Java
// Description : Implementation of graphics class


package com.asterdroids.framework.implementation;

import java.io.IOException;
import java.io.InputStream;

import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.content.Context;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import com.asterdroids.game.Assets;

import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Pixmap;

public class AndroidGraphics implements Graphics {

	AssetManager assets;           // Assets
	Bitmap frameBuffer;            // Framebuffer, for drawing before scaling
	Canvas canvas;                 // Canvas object to draw to
	Paint paint;                   // Paint object for primitive shapes
	Typeface font;                 // Font
	Rect srcRect = new Rect();     // Source and destination rects
	Rect dstRect = new Rect();
	
	public AndroidGraphics(AssetManager _assets, Bitmap _frameBuffer) {
		// Assign assets and framebuffer
		this.assets = _assets;
		this.frameBuffer = _frameBuffer;
		
		// Create canvas and paint objects
		this.canvas = new Canvas(frameBuffer);
		this.paint = new Paint();
	}
	
	@Override
	// Create a new Pixmap
	public Pixmap newPixmap(String fileName, PixmapFormat format) {
		Config config = null;  // Config info
		
		// Set config based on Pixmap format
		if(format == PixmapFormat.RGB565) {
			config = Config.RGB_565;
		}
		else if(format == PixmapFormat.ARGB4444) {
			config = Config.ARGB_4444;
		}
		else {
			config = Config.ARGB_8888;
		}
		
		// Assign options from config
		Options options = new Options();
		options.inPreferredConfig = config;
		
		// Stream and bitmap to read asset
		InputStream in = null;
		Bitmap bitmap = null;
		
		// Load asset into stream and read into bitmap
		// Throw exceptions if can't open asset
		try {
			in = assets.open(fileName);
			bitmap = BitmapFactory.decodeStream(in);
			if(bitmap == null)
				throw new RuntimeException("Couldn'e load bitmap from asset " + fileName);
		} catch(IOException e) {
			throw new RuntimeException("Couldn't load bitmap from asset " +fileName);
		} finally {
			if(in != null) {
				try {
					in.close();
				} catch (IOException e) {		
				}
			}
		}
		
		// Set formap based on config
		if(bitmap.getConfig() == Config.RGB_565) {
			format = PixmapFormat.RGB565;
		}
		else if(bitmap.getConfig() == Config.ARGB_4444) {
			format = PixmapFormat.ARGB4444;
		}
		else {
			format = PixmapFormat.ARGB8888;
		}
		
		// Return the loaded Pixmap
		return new AndroidPixmap(bitmap, format);
	}

	@Override
	// Clears the screen based on colour passed in
	public void clear(int color) {
		canvas.drawRGB((color & 0xff0000)>>16, (color & 0xff00)>>8, (color & 0xff));
	}

	@Override
	// Draw a pixel at a specified point
	public void drawPixel(int x, int y, int color) {
		paint.setColor(color);
		canvas.drawPoint(x, y, paint);
	}

	@Override
	// Draw text string using font
	public void drawTextString(int x,int y,int size, int color, String text, Context context){
		Assets.font = Typeface.createFromAsset(context.getAssets(), "font.ttf");
		paint.setTypeface(Assets.font);
		paint.setColor(color);
		paint.setTextSize(size);
		canvas.drawText(text, x, y, paint);
	}
	
	@Override
	// Draw line between two points
	public void drawLine(int x, int y, int x2, int y2, int color) {
		paint.setColor(color);
		canvas.drawLine(x, y, x2, y2, paint);
	}

	@Override
	// Draw rectangle primitive
	public void drawRect(int x, int y, int width, int height, int color) {
		paint.setColor(color);
		paint.setStyle(Style.FILL);
		canvas.drawRect(x, y, x+width-1, y+height-1, paint);
	}

	// Draw circle primitive
	public void drawCircle( float x, float y, float radius, int color ) {
		paint.setColor( color );
		paint.setStyle( Style.FILL );
		canvas.drawCircle( x, y, radius, paint );	
	}
	
	
	@Override
	// Draw Pixmap using rects
	public void drawPixmap(Pixmap pixmap, int x, int y, int srcX, int srcY,
			int srcWidth, int srcHeight) {
		srcRect.left = srcX;
		srcRect.top = srcY;
		srcRect.right = srcX + srcWidth - 1;
		srcRect.bottom = srcY + srcHeight - 1;
		
		dstRect.left = x;
		dstRect.top = y;
		dstRect.right = x + srcWidth -1;
		dstRect.bottom = y + srcHeight - 1;
		
		canvas.drawBitmap(((AndroidPixmap) pixmap).bitmap, srcRect, dstRect, null);
	}

	@Override
	// Draw pixmap at coordinates
	public void drawPixmap(Pixmap pixmap, int x, int y) {
		canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, x-pixmap.getWidth()/2, y-pixmap.getHeight()/2, null);
	}
	
	@Override
	// Draw pixmap with rotation at coordinates
	public void drawPixmap(Pixmap pixmap, int x, int y, float rot) {
		
		Matrix transform = new Matrix();
		//transform.setTranslate(-pixmap.getWidth()/2, -pixmap.getHeight()/2);
		transform.setTranslate(x-pixmap.getWidth()/2, y-pixmap.getHeight()/2);
		transform.preRotate(rot,pixmap.getWidth()/2,pixmap.getHeight()/2);
		
		
		//canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, x-pixmap.getWidth()/2, y-pixmap.getHeight()/2, null);
		canvas.drawBitmap(((AndroidPixmap)pixmap).bitmap, transform, null);
	}

	// Accessor method for width and height of frame buffer
	@Override
	public int getWidth() {
		return frameBuffer.getWidth();
	}

	@Override
	public int getHeight() {
		return frameBuffer.getHeight();
	}

}
