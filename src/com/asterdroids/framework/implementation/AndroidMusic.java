// File        : AndroidMusic.Java
// Description : Implementation of music class

package com.asterdroids.framework.implementation;

import java.io.IOException;

import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;

import com.asterdroids.framework.Music;

public class AndroidMusic implements Music, OnCompletionListener {
	MediaPlayer mediaPlayer;    // Media player object
	boolean isPrepared = false; // Flag denoting if music is ready to play
	
	// Constructor
	public AndroidMusic(AssetFileDescriptor assetDescriptor) {
		// Create media player and attempt to load music file into it
        // Throws an exception if file can't be loaded
		mediaPlayer = new MediaPlayer();
		try {
			mediaPlayer.setDataSource(assetDescriptor.getFileDescriptor(),
					assetDescriptor.getStartOffset(),
					assetDescriptor.getLength());
			mediaPlayer.prepare();
			isPrepared = true;
			mediaPlayer.setOnCompletionListener(this);
		} catch (Exception e) {
			throw new RuntimeException("Couldn't load music");
		}
	}

	// Play music
	@Override
	public void play() {
		if(mediaPlayer.isPlaying()) {
			return;  // Return if music already playing
		}
		
		// If the music is prepared, play it
		// If not, prepare it
		try {
			synchronized(this) {
				if(!isPrepared) {
					mediaPlayer.prepare();
				}
				mediaPlayer.start();
			}
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Stop music
	@Override
	public void stop() {
		mediaPlayer.stop();
		synchronized(this) {
			isPrepared = false;
		}
	}

	// Pause music if currently playing
	@Override
	public void pause() {
		if(mediaPlayer.isPlaying()) {
			mediaPlayer.pause();
		}
	}

	// Set whether the music track should loop or not
	@Override
	public void setLooping(boolean _looping) {
		mediaPlayer.setLooping(_looping);
	}

	// Set volumve of music track
	@Override
	public void setVolume(float _volume) {
		mediaPlayer.setVolume(_volume,_volume);
	}

	
	// Return whether music is playing or not
	@Override
	public boolean isPlaying() {
		return mediaPlayer.isPlaying();
	}

	// Return whether music is stopped or not
	@Override
	public boolean isStopped() {
		return !isPrepared;
	}

	// Return whether music is looping or not
	@Override
	public boolean isLooping() {
		return mediaPlayer.isLooping();
	}

	// Disposal functional, releases resources
	@Override
	public void dispose() {
		if(mediaPlayer.isPlaying()) {
			mediaPlayer.stop();
		}
		mediaPlayer.release();
	}

	@Override
	public void onCompletion(MediaPlayer arg0) {
		synchronized(this) {
			isPrepared = false;
		}
	}

}
