// File        : Screen.Java
// Description : Interface for asterdroids screen class
//               Represents screen displayed on device

package com.asterdroids.framework;

public abstract class Screen {
	protected final Game game;
	
	// Constructor, assigns game object
	public Screen(Game game) {
		this.game = game;
	}
	
	// Update screen (time-based)
	public abstract void update(float deltaTime);
	
	// Render screen (time-based)
	public abstract void present(float deltaTime);
	
	// Pause the game - used when app is sent to background etc.
	public abstract void pause();
	
	// Resume - used when app returns from background
	public abstract void resume();
	
	// Disposal function
	public abstract void dispose();
}
