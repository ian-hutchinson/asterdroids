// File        : Audio.Java
// Description : Interface for asterdroids audio framework 

package com.asterdroids.framework;

public interface Audio {

	// Create music object
	public Music newMusic(String filename);
	
	// Create sound object
	public Sound newSound(String filename);
}
