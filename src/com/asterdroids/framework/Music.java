// File        : Music.Java
// Description : Interface for asterdroids music class

package com.asterdroids.framework;

public interface Music {
	// Play music
	public void play();

	// Stop music
	public void stop();
	
	// Pause music
	public void pause();
	
	// Toggle function for loop functionality
	public void setLooping(boolean _looping);
	
	// Volume setter
	public void setVolume(float _volume);
	
	// Boolean functions determining whether music is playing/stopped/looping
	public boolean isPlaying();
	
	public boolean isStopped();
	
	public boolean isLooping();
	
	// Disposal function
	public void dispose();
}
