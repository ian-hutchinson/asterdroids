// File        : Input.Java
// Description : Interface for asterdroids input handling framework
//               Handles touch events and accelerometer

package com.asterdroids.framework;

import java.util.List;

public interface Input {
	
	// KeyEvent class
	// Recognises key presses such as press back key
	public static class KeyEvent {
		public static final int KEY_DOWN = 0;
		public static final int KEY_UP = 1;
		
		public int type;
		public int keyCode;
		public char keyChar;
	}
	
	// TouchEvent class
	// Recognises various touch types at screen coordinates
	public static class TouchEvent {
		public static final int TOUCH_DOWN = 0;
		public static final int TOUCH_UP = 1;
		public static final int TOUCH_DRAGGED = 2;
		
		public int type;
		public int x,y;
		public int pointer;
	}
	
	// Check is key is pressed
	public boolean isKeyPressed(int keyCode);
	
	// Check if touch event is present
	public boolean isTouchDown(int pointer);
	
	// Get x coordinate of touch
	public int getTouchX(int pointer);
	
	// Get y coordinate of touch
	public int getTouchY(int pointer);
	
	
	// Functions to get X, Y and Z value of accelerometer
	public float getAccelX();
	
	public float getAccelY();
	
	public float getAccelZ();
	
	// Return list of key events
	public List<KeyEvent> getKeyEvents();
	
	// Return list of touch events
	public List<TouchEvent> getTouchEvents();
}
