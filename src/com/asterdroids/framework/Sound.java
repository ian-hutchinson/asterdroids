// File        : Sound.Java
// Description : Interface for asterdroids sound class 


package com.asterdroids.framework;

public interface Sound {
	
	// Play sound at specified volume
	public void play(float volume);
	
	// Stop currently playing sound;
	public void stop ();
	
	// Disposal function
	public void dispose();
}
