// File        : CollidableObject.Java
// Description : Implementation of a class used as a base class for
//               objects that collide with each other

package com.asterdroids.game;

import com.asterdroids.framework.Vector2;

public class CollidableObject{
	public Vector2 position = new Vector2();  // Position
	public int radius;                        // Collision radius
	
	// Collision check function, uses radial collision, not box collision
	public boolean collidesWith(CollidableObject otherObj) {
		
		// Checl if radii are set
		if(this.radius != 0 && otherObj.radius != 0) {
			// If overlapping collision radii, collision has occurred
			if(this.position.distance(otherObj.position) < this.radius + otherObj.radius)
				return true;
			else
				return false;
		}
		return false;
	}
	

	
}