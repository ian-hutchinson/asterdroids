// File        : LargeAsteroid.java
// Description : A child class for the larger asteroids

package com.asterdroids.game.asteroids;

import com.asterdroids.framework.Game;
import com.asterdroids.game.Assets;
import com.asterdroids.game.asteroids.Asteroid;

public class LargeAsteroid extends Asteroid {
	//Constructor
	public LargeAsteroid(Game game) {
		// Set the graphics engine
		g = game.getGraphics();
		// Picks 0 or 1 for the selection of a large asteroid image
		int random = (int )(Math.random() * 2);
		// Selects an image and sets relevant radius
		if(random == 0) {
			AstPicture = Assets.asteroid_large_1;
			radius = 81;
		} else {
			AstPicture = Assets.asteroid_large_2;
			radius = 73;
		}
		
		// Sets direction
		random = (int )(Math.random() * 360 + 1);
		currentRotation = random;
		
		// Ensures that the asteroid is out of the "safe area" 
		float xPos, yPos;
		xPos = (int )(Math.random() * g.getWidth() + 1);
		while(xPos+AstPicture.getWidth() > g.getWidth()/2 - 55 && 
				xPos-AstPicture.getWidth() < g.getWidth()/2 + 55)
			xPos = (int )(Math.random() * g.getWidth() + 1);
		yPos = (int )(Math.random() * g.getHeight() + 1);
		position.set(xPos, yPos);
	
		// Sets the speed of the asteroid
		speed = 20;
		
		//Sets the speed and direction of the rotation
		tempRotation = currentRotation;
		random = (int)(Math.random()*2);
		if(random == 1)
		{
			rotationDirection = true;
		}
		else
		{
			rotationDirection = false;
		}
		rotationSpeed = (float)(Math.random()*1.5 +0.1);
		
		// A large asteroid is type 1
		type = 1;
	}
	
}