//File        : Asteroid.java
//Description : Parent class for all asteroids

package com.asterdroids.game.asteroids;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Pixmap;
import com.asterdroids.framework.Graphics;
import com.asterdroids.game.Assets;
import com.asterdroids.game.CollidableObject;

public class Asteroid extends CollidableObject{
	Graphics g;							// The graphics engine
	public float currentRotation;		// The direction of the asteroid
	public float speed;					// The speed of the asteroid
	public boolean rotationDirection;	// The rotation to make it spin
	public float rotationSpeed;			// The speed of the spinning
	public Pixmap AstPicture;			// The picture that the asteroid uses
	public float tempRotation;			// The point to which it has rotated
	public int type;					// The size of the asteroid
	
	public Asteroid(){
		//Default constructor
	}
	
	// Constructor to parent class
	public Asteroid(Game game){
		g = game.getGraphics();
		// Picks the large asteroid image
		AstPicture = Assets.asteroid_large_1;
		// Selects a random number between 1 and 360 for the direction of the asteroid
		int random = (int )(Math.random() * 360 + 1);
		currentRotation = random;
		
		// Randomly selects the position, outside of the players starting position
		float xPos, yPos;
		xPos = (int )(Math.random() * g.getWidth() + 1);
		while(xPos+AstPicture.getWidth() > g.getWidth()/2 - 55 && 
				xPos-AstPicture.getWidth() < g.getWidth()/2 + 55)
			xPos = (int )(Math.random() * g.getWidth() + 1);
		yPos = (int )(Math.random() * g.getHeight() + 1);
		position.set(xPos, yPos);	
		
		// Sets a speed
		speed = 2;
		
		
		tempRotation = currentRotation;
		
		//Sets the direction of rotation
		random = (int)(Math.random()*2);
		if(random == 1)
		{
			rotationDirection = true;
		}
		else
		{
			rotationDirection = false;
		}
		//Sets the speed of rotation
		rotationSpeed = (float)(Math.random()*1.5 +0.1);
	}
	
	// Update function for asteroids
	public void Update(float deltaTime){
		// Allows for wrapping around the screen
		if(position.x > g.getWidth()+(AstPicture.getWidth()))
		{
			position.x = 0 - AstPicture.getWidth()/2;
		}
		else if(position.x < 0 - AstPicture.getWidth())
		{
			position.x = g.getWidth() + (AstPicture.getWidth()/2);
		}
		if(position.y > g.getHeight() + AstPicture.getHeight())
		{
			position.y = 0 - AstPicture.getHeight()/2;
		}
		else if(position.y < 0 - AstPicture.getHeight())
		{
			position.y  = g.getHeight() + (AstPicture.getHeight()/2);
		}
		
		// Direction controls on the x and y axis
		if(currentRotation >= 0 && currentRotation <= 90)
		{
			position.x+=((currentRotation/90)) * speed * deltaTime;
			position.y+=(0-(1-currentRotation/90)) * speed * deltaTime;
		}
		else if(currentRotation > 90 && currentRotation <= 180)
		{
			position.x+=((1-(currentRotation-90)/90)) * speed * deltaTime;
			position.y+=((currentRotation-90)/90) * speed * deltaTime;
		}
		else if(currentRotation >180 && currentRotation <= 270)
		{
			position.x+=(0-((currentRotation-180)/90)) * speed * deltaTime;
			position.y+=((1-(currentRotation-180)/90)) * speed * deltaTime;
		}
		else if(currentRotation > 270 && currentRotation <= 360)
		{
			position.x+=(0-(1-(currentRotation-270)/90)) * speed * deltaTime;
			position.y-=(((currentRotation-270)/90)) * speed * deltaTime;
		}
		else if(currentRotation > 360)
		{
			currentRotation -= 360;
		}
	}
	
	// Draw function for asteroids
	public void draw(){
		g.drawPixmap(AstPicture, (int)position.x, (int)position.y, tempRotation);
		// Rotates the asteroid image
		if(rotationDirection)
		{
			tempRotation += rotationSpeed;
		}
		else
		{
			tempRotation -= rotationSpeed;
		}
			
	}
	
	
}