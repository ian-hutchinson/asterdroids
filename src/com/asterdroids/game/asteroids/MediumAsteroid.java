// File		   : MediumAsteroid.java
// Description : A child class for the medium asteroids

package com.asterdroids.game.asteroids;

import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;
import com.asterdroids.game.asteroids.Asteroid;

public class MediumAsteroid extends Asteroid {
	// Constructor
	public MediumAsteroid(Graphics graph, Vector2 startingPos, float Direction){
		// Graphics engine
		g = graph;
		// Uses random to select one of two asteroid images
		int random = (int )(Math.random() * 2);
		if(random == 0)
			AstPicture = Assets.asteroid_medium_1;
		else
			AstPicture = Assets.asteroid_medium_2;
		// Sets the radius
		radius = 36;

		// Sets the speed
		speed = 40;
		// Uses the position that was passed in for the starting position
		position = startingPos;
		
		// Sets a variance from the initial direction
		random = (int )(Math.random() * 45 + 1);
		int temp = random;

		// Sets a direction for the variance to be applied
		random = (int)(Math.random()*2);

		// Applies variance and sets rotationDirection
		if(random == 1)
		{
			currentRotation = (Direction + temp);
			rotationDirection = true;
		}
		else
		{
			currentRotation = (Direction - temp);
			rotationDirection = false;
		}
		
		// Sets rotation speed
		rotationSpeed = (float)(Math.random()*1.5 +0.1);
		tempRotation = currentRotation;
		
		// Sets asteroid type
		type = 2;		
	}
	
}