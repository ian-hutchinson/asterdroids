//File		  : AsteroidManager.java
//Description : Class to Manage the Asteroids on screen

package com.asterdroids.game.asteroids;

import com.asterdroids.game.Assets;
import com.asterdroids.game.Saucer;
import com.asterdroids.game.SmallSaucer;
import com.asterdroids.game.asteroids.Asteroid;
import com.asterdroids.game.asteroids.MediumAsteroid;
import com.asterdroids.game.asteroids.LargeAsteroid;
import com.asterdroids.game.asteroids.SmallAsteroid;
import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;

import java.util.ArrayList;
import java.util.List;

public class AsteroidManager {
	public List<Asteroid> Cluster = new ArrayList<Asteroid>();	// List of all of the asteroids
	Graphics g;													// The graphics engine
	public int Level;											// The current level of the game
	
	//Asteroid Manager Constructor
	public AsteroidManager(Game game){
		
		Level = 0;
		level(game);
		
		g = game.getGraphics();
	}
	
	//Level function - used to add the correct amount of Asteroids when a new level is reached
	public void level(Game game){	
		for (int i = 0 ; i < Level+4 ; i++)
		{
			//Adds a number of asteroids depending on level
			addLargeAsteroid(game);
		}
		//Increments the level up to 8
		if(Level < 8)
			Level++;		
	}

	//Update function for asteroids
	public void update(float deltaTime) {
		//Loops through the cluster and calls update
		for(int i = 0; i < Cluster.size(); i++) {
			Cluster.get(i).Update(deltaTime);
		}
	}
		
	//Draw function for asteroids
	public void draw() {
		//Loops through the cluster and calls draw
		for(int i = 0; i < Cluster.size(); i++) {
			Cluster.get(i).draw();
		}
	}
	
	//Adds a new large asteroid to the cluster
	public void addLargeAsteroid(Game game){
		Cluster.add(new LargeAsteroid(game));
	}
	
	//Removes an asteroid from the list
	public void destroyAsteroid(int toDestroy, Game game, Saucer saucer, SmallSaucer smallSaucer){
		if(toDestroy < Cluster.size()){
			//If it's a large Asteroid, add two medium ones
			if(Cluster.get(toDestroy).type == 1)
			{
				
				Cluster.add(new MediumAsteroid(g, Cluster.get(toDestroy).position, Cluster.get(toDestroy).currentRotation));
				Cluster.add(new MediumAsteroid(g, Cluster.get(toDestroy).position, Cluster.get(toDestroy).currentRotation));
			}
			//If it's a medium Asteroid, add two small ones
			if(Cluster.get(toDestroy).type == 2)
			{
				
				Cluster.add(new SmallAsteroid(g, Cluster.get(toDestroy).position, Cluster.get(toDestroy).currentRotation));
				Cluster.add(new SmallAsteroid(g, Cluster.get(toDestroy).position, Cluster.get(toDestroy).currentRotation));				
			}
			//Play sound
			Assets.boomSound.play(1);
			//Remove destroyed asteroid
			Cluster.remove(toDestroy);
		}
		//Goes to the next level twhen this cluster is destroyed
		if(Cluster.isEmpty()){
			
			level(game);
			saucer.reset();
			smallSaucer.reset();
			System.out.println("SAUCER RESET");
		}
	}
}
