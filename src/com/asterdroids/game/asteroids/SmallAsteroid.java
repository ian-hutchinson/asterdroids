// File		   : SmallAsteroid.java
// Description : A child class for the small asteroids

package com.asterdroids.game.asteroids;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;
import com.asterdroids.game.asteroids.Asteroid;

public class SmallAsteroid extends Asteroid {
	// Constructor
	public SmallAsteroid(Graphics graph, Vector2 startingPos, float Direction){
		// Graphics Engine
		g = graph;

		// Sets the relevant image
		AstPicture = Assets.asteroid_small;
		// Sets up the radius of the asteroid
		radius = 16;
		// Sets the starting position from the value that was passed
		position = startingPos;
		// Sets the asteroids speed
		speed = 60;
		
		// Sets a variance and rotation
		int random = (int )(Math.random() * 45 + 1);
		int temp = random;
		random = (int)(Math.random()*2);
		// Applies the variance and rotation
		if(random == 1)
		{
			currentRotation = (Direction + temp);
			rotationDirection = true;
		}
		else
		{
			currentRotation = (Direction - temp);
			rotationDirection = false;
		}
		// Sorts out the speed of rotation
		rotationSpeed = (float)(Math.random()*1.5 +0.1);
		tempRotation = currentRotation;
		
		//Sets asteroid type
		type = 3;		
	}	
}