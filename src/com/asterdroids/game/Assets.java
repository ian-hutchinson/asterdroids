// File        : Assets.Java
// Description : Class to store game assets

package com.asterdroids.game;

import com.asterdroids.framework.Pixmap;
import com.asterdroids.framework.Sound;

import android.graphics.Typeface;


public class Assets {
	public static Pixmap background;                    // Background image
	public static Pixmap title;                         // Title image
	public static Pixmap button_start;                  // Start button
	public static Pixmap button_settings;               // Settings button
	public static Pixmap button_main_menu;				// Main menu button
	public static Pixmap text_joystickControls;         // Joystick controls text
	public static Pixmap text_accelerometerControls;    // Accelerometer controls text
	public static Pixmap box_checked;                   // A checked box
	public static Pixmap box_unchecked;                 // An unchecked box
	public static Pixmap text_settingsTitle;            // Settings title
	public static Pixmap player_ship;                   // Ship graphic
	public static Pixmap player_ship_newLife;           // Blue ship graphic
	public static Pixmap ship_lives_icon;               // Live indicator icon
	public static Pixmap text_lives;                    // Lives text
	public static Pixmap text_score;                    // Score text
	public static Pixmap number_grid;                   // Number grid, used for score display
	public static Pixmap asteroid_large_1;              // Large asteroid
	public static Pixmap joystick_base;                 // Joystick base platform
	public static Pixmap joystick_handle;               // Joystick handle
	public static Pixmap bullet;                        // Bullet image
	public static Typeface font;                        // TrueType font
	public static Pixmap asteroid_large_2;              // Large asteroid
	public static Pixmap asteroid_medium_1;             // Medium asteroid
	public static Pixmap asteroid_medium_2;             // Medium asteroid
	public static Pixmap asteroid_small;                // Small asteroid
	public static Pixmap largeSaucer;                   // Large UFO
	public static Pixmap smallSaucer;                   // Small UFO
	public static Sound explosionSound;                 // Explosion SFX
	public static Sound shipSound;                      // Ship SFX
	public static Sound laserSound;                     // Laser SFX
	public static Sound boomSound;						// Explosion SFX
	public static Pixmap game_over;						//Game over text
}
