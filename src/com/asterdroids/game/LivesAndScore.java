// File        : LivesAndScore.Java
// Description : Class for displaying HUD elements, such as life count
//               and score to the screen


package com.asterdroids.game;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;

public class LivesAndScore {
	Graphics g;
	
	private int lives;  // Number of lives
	int score;          // Current score
	

	String scoreString;    // Score string
	Vector2 shipLivesPos;  // Position of lives display
	
	// Constuctor
	public LivesAndScore(Game game) {
		g = game.getGraphics();
		
		// Set lives and score
		lives = Settings.MAX_LIVES;
		score = 0;
		
		// Set live position
		shipLivesPos = new Vector2(0,0);
	}

	// Draw hud to screen
	public void draw() {
		// Draw lives string
		g.drawPixmap(Assets.text_lives, 60, 35);
		shipLivesPos.set(Assets.text_lives.getWidth() + 40,35);
		
		// Draw icon for each life remaining
		for(int i=0; i<getLives(); i++) {
			g.drawPixmap(Assets.ship_lives_icon, (int)shipLivesPos.x, (int)shipLivesPos.y);
			shipLivesPos.x += (Assets.ship_lives_icon.getWidth());
		}
		
		// Draw score
		drawScore();
	}
	
	// Change score
	public void scoreChange(int change) {
		score += change;
	}
	
	// Change lives
	public void livesChange(int change) {
		lives += change;
	}

	// Subtract lives
	public void subtractLives(int change) {
		lives -= change;
	}
	
	// Draw score
	private void drawScore() {
		
		// Draw text values
		g.drawPixmap(Assets.text_score, 1000, 35);
		int x = 1010 + Assets.text_score.getWidth()/2;
		scoreString = "" + score;
		int len = scoreString.length();
		
		// Draw score characters taken from score grid
		for(int i=0;i<len;i++) {
			char character = scoreString.charAt(i);
			
			g.drawPixmap(Assets.number_grid, x, 15, (character - '0') * 19, 0, 19, 41);
			x += 19;
		}
		
	}

	public void drawScoreAt(Vector2 pos) {
		//g.drawPixmap(Assets.text_score, (int)pos.x, (int)pos.y);

		scoreString = "" + score;
		int len = scoreString.length();
		for(int i=0;i<len;i++) {
			char character = scoreString.charAt(i);
			
			g.drawPixmap(Assets.number_grid, (int)pos.x, (int)pos.y, (character - '0') * 19, 0, 19, 41);
			pos.x += 19;
		}
	}

	// Accessor functions for lives and score
	public int getLives() {
		return lives;
	}
	
	public int getScore() {
		return score;
	}


}
