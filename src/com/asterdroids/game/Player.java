// File        : Player.Java
// Description : Implemenation of player class

package com.asterdroids.game;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Input;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.bullets.BulletPool;

public class Player extends CollidableObject {
	
	Graphics g;           // Graphics pointer
	Input input;          // Input pointer
	static int life;      // Lives
	float rot;            // Rotation value
	Vector2 velocity;     // Velocity vector
	Vector2 acceleration; // Acceleration vector

	public BulletPool bullets; //bullets


	private static final int MAX_SPEED = 1000; // Maximum ship speed
	
	// Constructor
	public Player(Game game) {
		// Set pointers to graphics and inputs
		g = game.getGraphics();
		input = game.getInput();
		
		// Initialise position, velocity and acceleration
		position.set(g.getWidth()/2, g.getHeight()/2);
		velocity = new Vector2(0,0);
		acceleration = new Vector2(0,0);
		
		// Create bullet pool
		bullets = new BulletPool( g );
	}
	
	// Update player
	public void update(Joystick leftStick,Joystick rightStick, float deltaTime) {
		
		// If not using joystick, read settings from accelerometer
		if(!Settings.joystickMovement) {
			acceleration.x = input.getAccelY()*10;
			acceleration.y = (input.getAccelX()-7)*10;
		}
		else {
			
			// Get settings from joystick
			acceleration.x = -(leftStick.getXValue());
			acceleration.y = -(leftStick.getYValue());
			
			acceleration.x += -(velocity.x/20);
			acceleration.y += -(velocity.y/20);
		}
		
		// Add acceleration
		velocity.add(acceleration);
		
		// Cap velocities
		if(velocity.x > MAX_SPEED)
			velocity.x = MAX_SPEED;
		if(velocity.x < -MAX_SPEED)
			velocity.x = -MAX_SPEED;
		if(velocity.y > MAX_SPEED)
			velocity.y = MAX_SPEED;
		if(velocity.y < -MAX_SPEED)
			velocity.y = -MAX_SPEED;
		
		// Add velocity to position. Multiply by time for smooth
		// frame rate between devices.
		position.x += velocity.x * deltaTime;
		position.y += velocity.y * deltaTime;
		
		// Wrap around screen if ship goes out of bounds
		if(position.x > g.getWidth())
			position.x = 0;
		if(position.x < 0)
			position.x = g.getWidth();
		
		if(position.y > g.getHeight())
			position.y = 0;
		if(position.y < 0)
			position.y = g.getHeight();
		
	
		//Rotate ship
		if(Settings.joystickMovement) {
			if(leftStick.getXValue()!=0 && leftStick.getYValue()!=0) {
			rot = acceleration.angle()+90;
			}
		} else {
			rot = acceleration.angle()+90;
		}

		
		// Update bullets
		bullets.Update(deltaTime);
	}
	
	// Draw ship to screen
	public void draw() {
		
		// Draw ship to screen
		g.drawPixmap(Assets.player_ship, (int)position.x, (int)position.y,rot);
		
		// Draw bullets
		bullets.Draw();
		
		radius = 60;
	}
	
	// Reset player
	public void playerReset(){
		g.drawPixmap(Assets.player_ship_newLife, (int)position.x, (int)position.y, rot);
		radius = 0;
	}
	
	// Shoot bullet
	public void Shoot( Vector2 target ) {
		bullets.Shoot( position, target );
	}
	
}
