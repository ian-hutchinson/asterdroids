// File        : SmallSaucer.Java
// Description : Class for a small saucer object
package com.asterdroids.game;

import com.asterdroids.framework.Game;
import com.asterdroids.game.Saucer;
import com.asterdroids.framework.Vector2;

public class SmallSaucer extends Saucer {

	// Constructor
	public SmallSaucer(Game game) {
		
		// Set graphics pointer and assets
		g = game.getGraphics();
		saucerPic = Assets.smallSaucer;
		radius = 50;
		reset();
	}
	
	
	// Shoot bullet
	public void shoot(Vector2 shipLoc, float deltaTime){
		// If alive and ready to shoot, shoot a bullet towards
		// the ship's current position
		if(alive){
			shotAcceleration.set(shipLoc);
			shotAcceleration.sub(position);
			if(cooldown >= 1) { 
				bullets.Shoot(position, shotAcceleration.normal());
				cooldown = cooldown - 1*deltaTime;
			} else if(cooldown < 0) {
				cooldown = 1;
			} else {
				cooldown = cooldown - 1*deltaTime;
			}
		}
	}
}