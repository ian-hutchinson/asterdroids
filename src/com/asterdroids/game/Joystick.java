// File        : Joystick.Java
// Description : Class to represent virtual, on-screen joystick


package com.asterdroids.game;

import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;

public class Joystick {
	private int x, y;           // Position coords
	private int tX, tY;         // Touch coords
	private float touchRadius;  // Touch radius
	
	
	// Constructor
	public Joystick( int posX, int posY, float tR ) {
		x = posX;
		y = posY;
		tX = posX;
		tY = posY;
		touchRadius = tR;	
	}
	
	// Draw the joystick to the screen
	public void Draw( Graphics g )	{
		// Draw the base
		g.drawPixmap( Assets.joystick_base, x, y );
		
		// Draw the handle
		g.drawPixmap( Assets.joystick_handle, tX, tY );
		
		
	}
	
	// Return whether touch is near joystick
	public boolean touchInJoystickBounds( int posX, int posY ) {
	
		// Check to see if on screen touch is inside of the circle created
		// by the touch radius for the joystick
		if( ( ( posX - x ) * ( posX - x ) ) + ( ( posY - y ) * (posY - y ) ) <= touchRadius * touchRadius ) {
			return true;
		}
		
		return false;
	}
	
	// Update handle position - sets the position of the
	// handle based on touch event coordinates
	public void updateHandlePosition( int hX, int hY ) {
		tX = hX;
		tY = hY;
	}
	
	// Set sticks back to centre position
	public void quickResetSticks() {
		tX = x;
		tY = y;
	}
	
	// Get x value of joystick (touch, not base position)
	public float getXValue() {
		return x - tX;
	}
	
	// Get y value of joystick (touch, not base position)
	public float getYValue() {
		return y - tY;
	}
	
	// Function to get the correct vector for bullet velocity
	public Vector2 getShotVector() {
		
		// Delicious, delicious vector math
		Vector2 b, t;
		b = new Vector2( x, y );
		t = new Vector2( tX, tY );
		
		// Subtract target from base, negate then normalise
		Vector2 ret = new Vector2( b.sub( t ) );
		ret.negate();
		ret.normal();
		
		return ret;
		
	}
}
