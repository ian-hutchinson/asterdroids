// File        : LoadingScreen.Java
// Description : Class to represent loading screen.
//               Although never displayed, a lot of settings are initialised here.

package com.asterdroids.game.screens;

import android.util.Log;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Graphics.PixmapFormat;
import com.asterdroids.framework.Screen;
import com.asterdroids.game.Assets;
import com.asterdroids.game.Settings;

public class LoadingScreen extends Screen {

	// Constructor
	public LoadingScreen(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
	}

	// Update, will effectively run once
	@Override
	public void update(float deltaTime) {
		Graphics g = game.getGraphics();
		
		// Load background and button assets
		Assets.background = g.newPixmap("background.png", PixmapFormat.ARGB4444);
		Assets.title = g.newPixmap("title.png", PixmapFormat.ARGB4444);
		Assets.button_start = g.newPixmap("button_start.png", PixmapFormat.ARGB4444);
		Assets.button_settings = g.newPixmap("button_settings.png", PixmapFormat.ARGB4444);	
		Assets.game_over = g.newPixmap("game_over.png", PixmapFormat.ARGB4444);
		Assets.button_main_menu = g.newPixmap("button_main_menu.png", PixmapFormat.ARGB4444);
		
		// Load settings assets
		Assets.text_settingsTitle = g.newPixmap("text_settings.png", PixmapFormat.ARGB4444);
		Assets.box_checked = g.newPixmap("checked_box.png", PixmapFormat.ARGB4444);
		Assets.box_unchecked = g.newPixmap("unchecked_box.png", PixmapFormat.ARGB4444);
		Assets.text_joystickControls = g.newPixmap("text_joystickcontrol.png", PixmapFormat.ARGB4444);
		Assets.text_accelerometerControls = g.newPixmap("text_accelcontrol.png", PixmapFormat.ARGB4444);
		
		// Load joystick assets
		Assets.joystick_base = g.newPixmap( "joystick_base.png", PixmapFormat.ARGB4444 );
		Assets.joystick_handle = g.newPixmap( "joystick_handle.png", PixmapFormat.ARGB4444 );
		
		// Load HUD assets
		Assets.text_lives = g.newPixmap("text_lives.png", PixmapFormat.ARGB4444);
		Assets.ship_lives_icon = g.newPixmap("lives_ship.png", PixmapFormat.ARGB4444);
		Assets.text_score = g.newPixmap("text_score.png", PixmapFormat.ARGB4444);
		Assets.number_grid = g.newPixmap("number_grid.png", PixmapFormat.ARGB4444);
		
		// Load bullet asset
		Assets.bullet = g.newPixmap( "bullet.png", PixmapFormat.ARGB4444 );
		
		// Load ship assets
		Assets.player_ship = g.newPixmap("player_ship.png", PixmapFormat.ARGB4444);
		Assets.player_ship_newLife = g.newPixmap("player_ship_newLife.png", PixmapFormat.ARGB4444);
		
		// Load asteroid assets
		Assets.asteroid_large_1 = g.newPixmap("asteroid_large_1edit.png", PixmapFormat.ARGB4444);
		Assets.asteroid_large_2 = g.newPixmap("asteroid_large_2edit.png", PixmapFormat.ARGB4444);
		Assets.asteroid_medium_1 = g.newPixmap("asteroid_medium_1edit.png", PixmapFormat.ARGB4444);
		Assets.asteroid_medium_2 = g.newPixmap("asteroid_medium_2edit.png", PixmapFormat.ARGB4444);
		Assets.asteroid_small = g.newPixmap("asteroid_smalledit.png", PixmapFormat.ARGB4444);

		
		// Load saucer assets
		Assets.largeSaucer = g.newPixmap("alien_edit.png", PixmapFormat.ARGB4444);
		Assets.smallSaucer = g.newPixmap("alien_smalledit.png", PixmapFormat.ARGB4444);
		
        // Load sound assets
		Assets.shipSound = game.getAudio().newSound("Ship.ogg");
		Assets.laserSound = game.getAudio().newSound("laser.ogg");
		Assets.boomSound = game.getAudio().newSound("boom.ogg");


		// Load control settings from file
		Settings.load(game.getFileIO());
		Log.v("LoadingScreen","Assets Loaded");
		
		// Jump to main menu
		game.setScreen(new MainMenuScreen(game));
	}

	@Override
	public void present(float deltaTime) {
		// TODO Auto-generated method stub

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
