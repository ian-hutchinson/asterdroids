// File        : SettingsScreen.Java
// Description : Class represeting settings screen

package com.asterdroids.game.screens;

import java.util.List;

import android.util.Log;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;
import com.asterdroids.framework.Input.KeyEvent;
import com.asterdroids.framework.Screen;
import com.asterdroids.framework.Input.TouchEvent;
import com.asterdroids.game.Assets;
import com.asterdroids.game.Settings;

public class SettingsScreen extends Screen {
	Graphics g = game.getGraphics(); // Graphics pointer
	
	Vector2 titlePosition = null;    // Title position
	Vector2 joystickpos = null;      // Joystick setting position
	Vector2 accelpos = null;         // Accelerometer setting position
	Vector2 joystickboxpos = null;   // Joystick checkbox position
	Vector2 accelboxpos = null;      // Accelerometer checkbox position
	
	
	// Constructor
	public SettingsScreen(Game game) {
		super(game);
		
		// Set text positions
		titlePosition = new Vector2(g.getWidth()/2, 200);
		joystickpos = new Vector2(400-(Assets.text_accelerometerControls.getWidth()/2-Assets.text_joystickControls.getWidth()/2),
				400);
		accelpos = new Vector2(400, 500);
		
		// Set checkbox positions
		joystickboxpos = new Vector2(1000, 400);
		accelboxpos = new Vector2(1000, 500);
	}

	@Override
	// Update, check for touchers
	public void update(float deltaTime) {
		
		// Check for keyboard events
		List<KeyEvent> keyboardEvents = game.getInput().getKeyEvents();
		int len = keyboardEvents.size();
		for(int i=0; i<len; i++) {
			KeyEvent event = keyboardEvents.get(i);
			
			// If the back key is pressed, save current settings and
			// return to main menu screen
			if(event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
				if(event.type == KeyEvent.KEY_DOWN) {
					Settings.save(game.getFileIO());
					game.setScreen(new MainMenuScreen(game));
				}
			}
		}
		
		// Check for touch events
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		len = touchEvents.size();
		for(int i=0;i<len;i++) {
			TouchEvent event = touchEvents.get(i);
			
			// If a touch release is found in either checkbox, set the joystick setting
			// accordingly.
			if(event.type == TouchEvent.TOUCH_UP) {
				if(inBounds(event, (int)joystickboxpos.x - Assets.box_checked.getWidth()/2,
						(int)joystickboxpos.y - Assets.box_checked.getHeight()/2,
						Assets.box_checked.getWidth(), Assets.box_checked.getHeight())) {
					Log.v("SettingsScreen","Joystick selected");
					Settings.joystickMovement = true;
				}
				else if(inBounds(event, (int)accelboxpos.x - Assets.box_checked.getWidth()/2,
						(int)accelboxpos.y - Assets.box_checked.getHeight()/2,
						Assets.box_checked.getWidth(), Assets.box_checked.getHeight())) {
					Log.v("SettingsScreen","Accelerometer selected");
					Settings.joystickMovement = false;
				}
			}
		}
	}

	@Override
	// Draw assets to screen
	public void present(float deltaTime) {
		// TODO Auto-generated method stub
		
		// Draw background and text
		g.drawPixmap(Assets.background, g.getWidth()/2, g.getHeight()/2);
		g.drawPixmap(Assets.text_settingsTitle, (int)titlePosition.x, (int)titlePosition.y);
		g.drawPixmap(Assets.text_joystickControls, (int)joystickpos.x, (int)joystickpos.y);
		g.drawPixmap(Assets.text_accelerometerControls, (int)accelpos.x, (int)accelpos.y);
		
		// If joystick movement is enabled, ensure the joystick checkbox
		// is checked and the accelerometer box isn't.
		// Otherwise, draw the other way around.
		if(Settings.joystickMovement) {
			g.drawPixmap(Assets.box_checked, (int)joystickboxpos.x, (int)joystickboxpos.y);
			g.drawPixmap(Assets.box_unchecked, (int)accelboxpos.x, (int)accelboxpos.y);
		} else if(!Settings.joystickMovement) {
			g.drawPixmap(Assets.box_unchecked, (int)joystickboxpos.x, (int)joystickboxpos.y);
			g.drawPixmap(Assets.box_checked, (int)accelboxpos.x, (int)accelboxpos.y);
		}
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	// Check to see if touch is inside specified rectangle.
	// If it is, return true, otherwise return false.
	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		if(event.x > x && event.x < x + width - 1 &&
				event.y > y && event.y < y + height - 1)
			return true;
		else
			return false;
	}

}
