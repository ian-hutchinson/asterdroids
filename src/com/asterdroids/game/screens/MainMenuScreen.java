// File        : MainMenuScreen.Java
// Description : Class representing main menu screen

package com.asterdroids.game.screens;

import java.util.List;

import android.util.Log;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Input.KeyEvent;
import com.asterdroids.framework.Input.TouchEvent;
import com.asterdroids.framework.Screen;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;

public class MainMenuScreen extends Screen {

	Vector2 titlePosition = null;           // Title position
	Vector2 startButtonPosition = null;     // Start button position
	Vector2 settingsButtonPosition = null;  // Setting button position
	Graphics g = game.getGraphics();        // Graphics pointer
	
	
	// Constructor
	public MainMenuScreen(Game game) {
		super(game);
		// TODO Auto-generated constructor stub
		
		// Set button positions
		titlePosition = new Vector2(g.getWidth()/2, 200);
		startButtonPosition = new Vector2(g.getWidth()/2, 430);
		settingsButtonPosition = new Vector2(g.getWidth()/2, 570);
	}

	@Override
	// Update
	// Checks for touch events
	public void update(float deltaTime) {
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		int len = touchEvents.size();
		for(int i=0;i<len;i++) {
			TouchEvent event = touchEvents.get(i);
			// If touch event found and in bounds of button then
			// set the current screen to the appropriate screen
			if(event.type == TouchEvent.TOUCH_UP) {
				if(inBounds(event, (int)startButtonPosition.x - Assets.button_start.getWidth()/2,
						(int)startButtonPosition.y - Assets.button_start.getHeight()/2,
						Assets.button_start.getWidth(), Assets.button_start.getHeight())) {
					Log.v("MainMenuScreen","Start Clicked");
					game.setScreen(new GameScreen(game));
				}
				else if(inBounds(event, (int)settingsButtonPosition.x - Assets.button_settings.getWidth()/2,
						(int)settingsButtonPosition.y - Assets.button_settings.getHeight()/2,
						Assets.button_settings.getWidth(), Assets.button_settings.getWidth())) {
					Log.v("MainMenuScreen","Settings Clicked");
					game.setScreen(new SettingsScreen(game));
				}
			}
		}
		
		// Check for back key press
		// If found, the game will close
		List<KeyEvent> keyboardEvents = game.getInput().getKeyEvents();
		len = keyboardEvents.size();
		for(int i=0; i<len; i++) {
			KeyEvent event = keyboardEvents.get(i);
			if(event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
				if(event.type == KeyEvent.KEY_DOWN) {
					game.close();
				}
			}
		}
	}

	
	@Override
	// Draws graphics to screen
	public void present(float deltaTime) {
		Graphics g = game.getGraphics();
		
		// Draw background, title and bullets
		g.drawPixmap(Assets.background, g.getWidth()/2, g.getHeight()/2);
		g.drawPixmap(Assets.title, (int)titlePosition.x, (int)titlePosition.y);
		g.drawPixmap(Assets.button_start, (int)startButtonPosition.x, (int)startButtonPosition.y);
		g.drawPixmap(Assets.button_settings, (int)settingsButtonPosition.x, (int)settingsButtonPosition.y);
		
		//Log.v("MainMenuScreen", "Main menu present()");
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}
	
	// Check if touch is within a certain rectangle
	// If it is, return true, otherwise return false
	private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
		if(event.x > x && event.x < x + width - 1 &&
				event.y > y && event.y < y + height - 1)
			return true;
		else
			return false;
	}

}
