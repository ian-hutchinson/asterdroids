// File			: GameOverScreen.Java
// Description	: Class which shows the game over screen


package com.asterdroids.game.screens;

import java.util.List;

import android.util.Log;

import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Input.KeyEvent;
import com.asterdroids.framework.Input.TouchEvent;
import com.asterdroids.framework.Screen;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;
import com.asterdroids.game.LivesAndScore;

public class GameOverScreen extends Screen {

		Vector2 gameOverTextPosition = null;	//Set the position of the game over text to NULL
		Vector2 scoreTextPosition = null;		//Set position of the score text to NULL
		Vector2 mainMenuButtonPosition = null;	//Set position of the MainMenuButton to NULL
		Vector2 scorePos = new Vector2();       //Set the position of the actual score to NULL
		
		//Create a new LivesAndScore class object
		LivesAndScore lAndS;
		
		//Get an instance of the game graphics
		Graphics g = game.getGraphics();
		
		//Constructor
		public GameOverScreen(Game game, LivesAndScore livesAndScore) {
			super(game);
			// Set position of all text and button position s in constructor
			gameOverTextPosition = new Vector2(g.getWidth()/2, 200);
			scoreTextPosition = new Vector2(g.getWidth()/2 - 50, 350);
			mainMenuButtonPosition = new Vector2(g.getWidth()/2, 500);
			lAndS = livesAndScore;
		}


		//Function that updates each frame to check for touch events on the main
		//menu button
		@Override
		public void update(float deltaTime) {
			//Create a list of touch events 
			List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
			
			//Add to the list of touch events
			int len = touchEvents.size();
			for(int i=0;i<len;i++) {
				TouchEvent event = touchEvents.get(i);
				
				//If the touch event type equals the TOUCH_UP event and the touch in within
				//bounds of the button, get a new MainMenuScreen
				if(event.type == TouchEvent.TOUCH_UP) {
					if(inBounds(event, (int)mainMenuButtonPosition.x - Assets.button_main_menu.getWidth()/2,
							(int)mainMenuButtonPosition.y - Assets.button_main_menu.getHeight()/2,
							Assets.button_main_menu.getWidth(), Assets.button_main_menu.getHeight())) {
						Log.v("GameOverScreen","MainMenu Clicked");
						game.setScreen(new MainMenuScreen(game));
					}
				}
			}
			
			//Create a list of Android keyboard events and add to them
			//If the event equals the KEYCODE_BACK close the game.
			List<KeyEvent> keyboardEvents = game.getInput().getKeyEvents();
			len = keyboardEvents.size();
			for(int i=0; i<len; i++) {
				KeyEvent event = keyboardEvents.get(i);
				if(event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
					if(event.type == KeyEvent.KEY_DOWN) {
						game.close();
					}
				}
			}
		}

		
		//Draws all the buttons, text and images to the screen
		@Override
		public void present(float deltaTime) {
			Graphics g = game.getGraphics();
			scorePos.set(600, 330);
			
			
			g.drawPixmap(Assets.background, g.getWidth()/2, g.getHeight()/2);
			g.drawPixmap(Assets.game_over, (int)gameOverTextPosition.x, (int)gameOverTextPosition.y);
			g.drawPixmap(Assets.text_score, (int)scoreTextPosition.x, (int)scoreTextPosition.y);
			g.drawPixmap(Assets.button_main_menu, (int)mainMenuButtonPosition.x, (int)mainMenuButtonPosition.y);
			lAndS.drawScoreAt(scorePos);
			
			
			//Log.v("MainMenuScreen", "Main menu present()");
		}

		@Override
		public void pause() {
			// TODO Auto-generated method stub

		}

		@Override
		public void resume() {
			// TODO Auto-generated method stub

		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub

		}
		
		//Finds out if a touch was within a specific bounds
		private boolean inBounds(TouchEvent event, int x, int y, int width, int height) {
			if(event.x > x && event.x < x + width - 1 &&
					event.y > y && event.y < y + height - 1)
				return true;
			else
				return false;
		}

	}


