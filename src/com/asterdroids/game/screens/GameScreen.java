// File        : GameScreen.Java
// Description : Class to handle game logic and update
//               function

package com.asterdroids.game.screens;

import java.util.List;


import android.content.Context;
import com.asterdroids.framework.Game;
import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Screen;
import com.asterdroids.framework.Input.KeyEvent;
import com.asterdroids.framework.Input.TouchEvent;
import com.asterdroids.game.Assets;
import com.asterdroids.game.LivesAndScore;
import com.asterdroids.game.Joystick;
import com.asterdroids.game.Settings;
import com.asterdroids.game.Player;
import com.asterdroids.game.Saucer;
import com.asterdroids.game.SmallSaucer;

import com.asterdroids.game.asteroids.AsteroidManager;
import com.asterdroids.game.bullets.BulletPool;

public class GameScreen extends Screen {
	Graphics g = game.getGraphics();
	Player player;
	Joystick leftStick;
	Joystick rightStick; // Could be an array but I was getting NPEs...

	public Context context;
	public int counter = 0;
	AsteroidManager cluster;
	Saucer saucer;
	SmallSaucer smallSaucer;
	LivesAndScore livesAndScore;
	
	//create new game objects
	public GameScreen(Game game) {
		super(game);
		context = (Context) game;
			
		float r = Math.min( g.getWidth(), g.getHeight());
		
		leftStick = new Joystick( 150, 580 , r * 0.2f );
		rightStick = new Joystick( 1050, 580, r * 0.2f );
			
		player = new Player(game);

		saucer = new Saucer(game);
		
		cluster = new AsteroidManager(game);	
		
		smallSaucer = new SmallSaucer(game);
			
		livesAndScore = new LivesAndScore(game);

		//checks for all asteroids destroyed, then sets the level
		if(cluster.Cluster.isEmpty())
		{
			cluster.level(game);
			
		}
	}


	//update function which checks controls interactions
	@Override
	public void update(float deltaTime) {
		//sets the back key to send player back to main menu
		List<KeyEvent> keyboardEvents = game.getInput().getKeyEvents();
		int len = keyboardEvents.size();
		for(int i=0; i<len; i++) {
			KeyEvent event = keyboardEvents.get(i);
			if(event.keyCode == android.view.KeyEvent.KEYCODE_BACK) {
				if(event.type == KeyEvent.KEY_DOWN) {
					game.setScreen(new MainMenuScreen(game));
				}
			}
		}
		
		
		List<TouchEvent> touchEvents = game.getInput().getTouchEvents();
		len = touchEvents.size();
		for( int i=0; i < len; i++ ) {
			TouchEvent event = touchEvents.get(i);
			if( event.type == TouchEvent.TOUCH_DRAGGED ) {
				// Process joystick touches here
				if( Settings.joystickMovement ) {
					if( leftStick.touchInJoystickBounds( event.x, event.y) ) {
						leftStick.updateHandlePosition( event.x, event.y );
					}
				}
				//shooting controls
				if( rightStick.touchInJoystickBounds( event.x, event.y ) ) {
					rightStick.updateHandlePosition( event.x, event.y );		
					player.Shoot( rightStick.getShotVector() );	
				}
			} else {
				if( Settings.joystickMovement )

					leftStick.quickResetSticks();
				rightStick.quickResetSticks();
			}	
		}
		

		//Collision between player and asteroids
		for(int i = 0; i < cluster.Cluster.size(); i++){
			
			//checks for collision asteroids with player, and adjusts scores
			if(player.collidesWith(cluster.Cluster.get(i)) && livesAndScore.getLives() > 0){
				cluster.destroyAsteroid(i, game, saucer, smallSaucer);
				if (cluster.Cluster.get(i).type == 1)
					livesAndScore.scoreChange(20);
				else if (cluster.Cluster.get(i).type == 2)
					livesAndScore.scoreChange(50);		
				else if (cluster.Cluster.get(i).type == 3)
					livesAndScore.scoreChange(100);
			
				livesAndScore.subtractLives(1);
				counter = 200;		
			}
		}
		//Collision between player and saucer
		if(player.collidesWith(saucer) && livesAndScore.getLives() > 0){
			livesAndScore.subtractLives(1);
			counter = 200;
			}   
			
		//Collision between shots and asteroids
		for(int j = 0; j < cluster.Cluster.size(); j++){
			for(int i = 0; i < BulletPool.MAX_BULLETS; i++){
				if(j <= cluster.Cluster.size()){
					if(player.bullets.theBullets[i].collidesWith(cluster.Cluster.get(j))){
								
						if (cluster.Cluster.get(j).type == 1)
							livesAndScore.scoreChange(20);
						else if (cluster.Cluster.get(j).type == 2)
						  	livesAndScore.scoreChange(50);
					    else if (cluster.Cluster.get(j).type == 3)
					    	livesAndScore.scoreChange(100);
						cluster.destroyAsteroid(j, game, saucer, smallSaucer);
						break;
					}		
				}
			}
		}
				
			
				
		//Collision between shots and saucer and saucer shots and player
		for(int i = 0; i < BulletPool.MAX_BULLETS; i++){
			if(saucer.collidesWith(player.bullets.theBullets[i]) && saucer.alive){
				saucer.reset();
				saucer.alive = false;
				livesAndScore.scoreChange(200);
			}
			//if player shoots small saucer
			if(smallSaucer.collidesWith(player.bullets.theBullets[i]) && smallSaucer.alive){
				smallSaucer.reset();
				smallSaucer.alive = false;
				livesAndScore.scoreChange(1000);
			}
			//check for player shot by saucer
			if(saucer.bullets.theBullets[i].collidesWith(player)){
				livesAndScore.subtractLives(1);
				counter = 200;
			}
			//check for player shot by small saucer
			if(smallSaucer.bullets.theBullets[i].collidesWith(player)){
				livesAndScore.subtractLives(1);
				counter = 200;
			}
		}
		
		//Update player
		player.update(leftStick, rightStick, deltaTime);
		
		//Update for asteroids and saucers
		cluster.update(deltaTime);		
		saucer.update(deltaTime);
		saucer.shoot(deltaTime);
		smallSaucer.update(deltaTime);
		smallSaucer.shoot(deltaTime);

		//player.update(leftStick);
		if(livesAndScore.getLives() == 0){
			game.setScreen(new GameOverScreen(game, livesAndScore));
		}
	}
	//handles the drawing of each object
	@Override
	public void present(float deltaTime) {
		g.drawPixmap(Assets.background,g.getWidth()/2,g.getHeight()/2);
		if (counter > 0)
		{
			player.playerReset();
			counter -= 1 * deltaTime;
		}
		else
			player.draw();
		//if joystick controls enabled, draw the movement stick
		if( Settings.joystickMovement )
			leftStick.Draw( g );
		
		//draw the rest of the on screen objects
		rightStick.Draw( g );
		cluster.draw();
		saucer.draw();
		smallSaucer.draw();
		livesAndScore.draw();
	}
	//methods for when the game activity is paused
	@Override
	public void pause() {
		Assets.shipSound.stop();
		Assets.laserSound.stop();
		Assets.boomSound.stop();
	

	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		

	}

}
