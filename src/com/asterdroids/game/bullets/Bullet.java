// File        : Bullet.Java
// Description : Class used for bullet object


package com.asterdroids.game.bullets;

import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;
import com.asterdroids.game.CollidableObject;

public class Bullet extends CollidableObject {
	private boolean isActive;                    // Is the bullet active?
	private Vector2 velocity;                    // Velocity vector
	private static final int BULLET_SPEED = 20;  // Bullet speed
	
	// Constructor
	public Bullet( int x, int y, boolean active ) {
		// Set position, active state and velocity vector
		position.x = x;
		position.y = y;
		isActive = active;
		velocity = new Vector2( 0, 0 );
		radius = 1;
	}
	
	// Draw function
	// Draws the bullet to the screen if it's active
	public void Draw( Graphics g ) {
		if( isActive ) {
			g.drawPixmap( Assets.bullet, (int)position.x, (int)position.y );
		}
	}
	
	// Update the bullet
	public void Update( Graphics g ) {
		
		if( !isActive ) // No point processing inactive bullets
			return;
		
		// Bullet is active, update position
		position.add( velocity );
		
		// If the bullet goes off-screen, reset it
		if( position.x < 0 || position.x > g.getWidth() ||
		    position.y < 0 || position.y > g.getHeight() ) {
			
			isActive = false;
			position.x = 0;
			position.y = 0;
		}
	}

	// Spawns a bullet at a specified vector
	public void PlaceAt( Vector2 p, Vector2 v ) {
		position.set( p );             // Set initial bullet position
		velocity.set( v );             // Set bullet velocity (effectively direction)
		velocity.mul( BULLET_SPEED );  // Set bullet speed
		
		isActive = true;               // Make bullet active
	}
	
	// Return whether or not bullet is active
	public boolean IsActive() {
		return isActive;
	}
}

