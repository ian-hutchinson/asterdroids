// File        : BulletPool.Java
// Description : Collection class for bullet objects

package com.asterdroids.game.bullets;


import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Vector2;
import com.asterdroids.game.Assets;

public class BulletPool {
	public Bullet[] theBullets;                         // Array of bullets
	private Graphics g;                                  // Graphics pointer
	public static final int MAX_BULLETS = 15;           // Max bullets in container
	private static final float BULLET_COOLDOWN = 0.5f;   // Cooldown limit
	private float cooldown;                              // Bullet cooldown time
	
	// Constructor
	public BulletPool( Graphics graphics ) {
		
		// Initialse graphics and cooldown
		g = graphics;
		cooldown = 0.0f;
		
		// Assign memory for container
		theBullets = new Bullet[MAX_BULLETS];
		
		// Initialise the bullet pool
		for( int i=0; i < MAX_BULLETS; i++ ) {
			theBullets[i] = new Bullet( 50, 50, false );
		}
	}
	
	// Draw
	// Loops through bullets in container and draws (if active)
	public void Draw() {
		for( int i=0; i < MAX_BULLETS; i++ ) {
			theBullets[i].Draw( g );
		}
	}
	
	// Update
	public void Update(float deltaTime) {
		// Update cooldown time, if needed
		if( cooldown > 0 ) {
			cooldown -= 2f * deltaTime;
		}
		
		// Update each bullet
		for( int i=0; i < MAX_BULLETS; i++ ) {
			theBullets[i].Update( g );
		}
	}
	
	
	// Shoot bullet
	public void Shoot( Vector2 p, Vector2 v ) {
		
		// Regulate time between shots
		if( cooldown > 0 ) {
			
			return;
			
		}
		Assets.laserSound.play(1);
		
		// Sentinel used to find bullet
		int found = -1;
		
		// Loop through bullets to find inactive bullet available for use
		for( int i=0; i < MAX_BULLETS; i++ ) {
			if( !theBullets[i].IsActive() ) {
				found = i;
				break;
			}
		}
		
		// We have a free bullet
		// Spawn bullet at position, reset cooldown
		if( found != -1 ) {
			theBullets[found].PlaceAt( p, v );
			cooldown = BULLET_COOLDOWN;
			return;
		}
	}
	
}
