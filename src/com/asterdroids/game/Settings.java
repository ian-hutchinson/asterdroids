// File        : Settings.Java
// Description : Class to store game settings

package com.asterdroids.game;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import android.util.Log;

import com.asterdroids.framework.FileIO;

public class Settings {
	public static boolean joystickMovement = true; // Flag to denote if joystick control is enabled
	public static boolean soundEnabled = true;     // Flag to denote if sound is enabled
	public static final int MAX_LIVES = 3;         // Maximum number of lives
	
	// Load joystick movement preference from settings file
	// Throw an exception if file can't be read
	public static void load(FileIO files) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new InputStreamReader(
					files.readFile(".asterdroids")));
			joystickMovement = Boolean.parseBoolean(in.readLine());
		} catch (IOException e) {
			
		} catch (NumberFormatException e) {
			
		} finally {
			try {
				if(in != null)
					in.close();
			} catch(IOException e) {
				
			}
		}
	}
	
	// Save joystick preferences to file
	// If file can't be opened, throw exception
	public static void save(FileIO files) {
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new OutputStreamWriter(
					files.writeFile(".asterdroids")));
			out.write(Boolean.toString(joystickMovement));
			Log.v("Settings","File written");
		} catch(IOException e) {
			Log.v("Settings","File can't be written");
		} finally {
			try {
				if(out !=null)
					out.close();
			} catch(IOException e) {
				
			}
		}
	}
	
}
