// File        : AsteroidsGame.Java
// Description : Implementation of AndroidGame class

package com.asterdroids.game;

import com.asterdroids.framework.Screen;
import com.asterdroids.framework.implementation.AndroidGame;
import com.asterdroids.game.screens.LoadingScreen;

public class AsterdroidsGame extends AndroidGame {
	
	// Return start screen
	public Screen getStartScreen() {
		return new LoadingScreen(this);
	}
	
	// Action if back button pressed
	@Override
	public void onBackPressed() {
	}	
}