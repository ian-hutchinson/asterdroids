// File        : Saucers.Java
// Description : Implementation of the flying saucer class

package com.asterdroids.game;

import com.asterdroids.game.CollidableObject;
import com.asterdroids.game.bullets.BulletPool;

import com.asterdroids.framework.Graphics;
import com.asterdroids.framework.Pixmap;
import com.asterdroids.framework.Vector2;
import com.asterdroids.framework.Game;

public class Saucer extends CollidableObject {

	//public Vector2 position = new Vector2();          // Position
	public Vector2 shotAcceleration = new Vector2();  // Bullet acceleration
	public Pixmap saucerPic;                          // Saucer image
	public boolean alive;                             // Alive flag
	Graphics g;                                       // Graphics pointer
	public BulletPool bullets;                               // Bullets
	float cooldown;                                   // Bullet cooldown
	
	public Saucer() {
	
	}
	
	// Constructor
	public Saucer(Game game) {
		// Assign graphics pointer and asset
		g = game.getGraphics(); 
		saucerPic = Assets.largeSaucer;
		radius = 100;
		reset();
	}
	
	// Reset saucer
	public void reset() {
		// Set to a random point off screen
		int height = (int)(Math.random()*(g.getHeight()-saucerPic.getHeight()) + 1);
		position.set(-200, height);
		
		// Assign bullet pool
		bullets = new BulletPool(g);
		
		// Set cooldown and alive values
		cooldown = 1;
		alive = true;
	}
	
	// Update
	public void update(float deltaTime) {
		if(alive){
			// If saucer is alive, update position and bullets
			position.x = position.x + 200 * deltaTime;
			bullets.Update(deltaTime);
		}
		if(position.x > g.getWidth()+250) {
			alive = false;
		}
	}
	
	// Draw
	public void draw(){
		if(alive){
			// If saucer is alive, draw saucer and bullets
			g.drawPixmap(saucerPic, (int)position.x, (int)position.y);
			bullets.Draw();
		}
	}
	
	// Shoot bullet
	public void shoot(float deltaTime){
		if(alive){
			if(cooldown >= 1) {
				// If alive and ready to fire
				float randomX, randomY;
				randomX = (float)(Math.random()*3) -1;
				randomY = (float)(Math.random()*3) -1;
				shotAcceleration.set(randomX, randomY);
				
				// Fire bullet at generated point
				bullets.Shoot(position, shotAcceleration);
				
				// Set cooldown timer
				cooldown = cooldown - 1*deltaTime;
			} else if(cooldown < 0) {
				cooldown = 1;
			} else {
				cooldown = cooldown - 1*deltaTime;
			}
		}
	}
	
}